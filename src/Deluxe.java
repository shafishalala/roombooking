public class Deluxe extends Room {
    private int numberOfBeds;

    public Deluxe(int roomNumber, double nightlyRate, int numberOfBeds) {
        super(roomNumber, nightlyRate);
        this.numberOfBeds = numberOfBeds;
    }

    @Override
    public double calculateCharges(int numOfNights) {
        return getNightlyRate() * numOfNights;
    }

    @Override
    public boolean isAvailable() {
        return !isBooked();
    }
}