public class Suite extends Room {
    private int numberOfBeds;
    private boolean hasLivingRoom;

    public Suite(int roomNumber, double nightlyRate, int numberOfBeds, boolean hasLivingRoom) {
        super(roomNumber, nightlyRate);
        this.numberOfBeds = numberOfBeds;
        this.hasLivingRoom = hasLivingRoom;
    }

    @Override
    public double calculateCharges(int numOfNights) {
        return getNightlyRate() * numOfNights;
    }

    @Override
    public boolean isAvailable() {
        return !isBooked();
    }
}