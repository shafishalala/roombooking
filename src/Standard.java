public class Standard extends Room {
    public Standard(int roomNumber, double nightlyRate) {
        super(roomNumber, nightlyRate);
    }

    @Override
    public double calculateCharges(int numOfNights) {
        return getNightlyRate() * numOfNights;
    }

    @Override
    public boolean isAvailable() {
        return !isBooked();
    }
}