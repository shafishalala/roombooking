public abstract class Room {
    private int roomNumber;
    private double nightlyRate;
    private boolean booked;

    public Room(int roomNumber, double nightlyRate) {
        this.roomNumber = roomNumber;
        this.nightlyRate = nightlyRate;
        this.booked = false;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public double getNightlyRate() {
        return nightlyRate;
    }

    public boolean isBooked() {
        return booked;
    }

    public void bookRoom() {
        this.booked = true;
    }

    public void checkoutRoom() {
        this.booked = false;
    }

    public abstract double calculateCharges(int numOfNights);

    public abstract boolean isAvailable();
}