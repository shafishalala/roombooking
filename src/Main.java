public class Main {
    public static void main(String[] args) {
        Room[] rooms = new Room[5];
        rooms[0] = new Standard(101, 100.0);
        rooms[1] = new Deluxe(201, 150.0, 2);
        rooms[2] = new Suite(301, 250.0, 2, true);
        rooms[3] = new Standard(102, 100.0);
        rooms[4] = new Suite(302, 250.0, 2, false);

        // Booking Example
        rooms[0].bookRoom();
        rooms[2].bookRoom();

        // Checking Availability
        for (Room room : rooms) {
            System.out.println("Room " + room.getRoomNumber() + " Availability: " + room.isAvailable());
        }

        // Calculating Charges
        int numOfNights = 3;
        double totalCharges = 0.0;
        for (Room room : rooms) {
            if (room.isBooked()) {
                totalCharges += room.calculateCharges(numOfNights);
            }
        }
        System.out.println("Total Charges for " + numOfNights + " nights: $" + totalCharges);
    }
}